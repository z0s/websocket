<?php

namespace z0s\Commands;

use Bloatless\WebSocket\Server;
use Composer\Autoload\ClassLoader;
use Kcs\ClassFinder\Finder\ComposerFinder;
use League\Container\Container;
use z0s\Console\Api\ConsoleCommand;

class WebsocketServer extends ConsoleCommand
{
    public string $signature = 'websocket:server
        {--host=0.0.0.0 : Host to listen on }
        {--port= : Port to listen on }
        {--maxClients=100 : Max amount of clients to handle on this instance }
        {--checkOrigin=false : Check the origin }
        {--allowedOrigin= : The origin that is allowed, for example, example.com }
        {--maxConnectionsPerIp=20 : Maximum amount of connections from a single IP address }
    ';
    public string $description = 'Starts up a websocket server';

    public function __construct(
        protected ClassLoader $classLoader,
        protected Container $container
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $host = (string) $this->host ?? '0.0.0.0';
        $port = (int) $this->port ?? 8000;
        $maxClients = (int) $this->maxClients ?? 100;
        $checkOrigin = $this->checkOrigin === 'true' ?? false;
        $allowedOrigin = (string) $this->allowedOrigin ?? '';
        $maxConnectionsPerIp = (int) $this->maxConnectionsPerIp ?? 20;

        $this->out('Starting websocket server on port ' . $port);

        $uniqeId = uniqid();
        $server = new Server($host, $port, \BASE_DIR . "/cache/{$uniqeId}.sock");

        // Add logger

        // Settings
        $server->setMaxClients($maxClients);
        $server->setCheckOrigin($checkOrigin);
        $server->setAllowedOrigin($allowedOrigin);
        $server->setMaxConnectionsPerIp($maxConnectionsPerIp);

        // Find all websocket applications
        $finder = new ComposerFinder($this->classLoader);
        $finder->inNamespace('z0s\Websockets');

        foreach ($finder as $className => $reflection) {
            /** @var \z0s\Websocket\Api\Websocket $instance */
            $instance = $className::getInstance();
            $host = $instance->getHost();

            $server->registerApplication($host, $instance);
        }

        $server->run();
    }
}
