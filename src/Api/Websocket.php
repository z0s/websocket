<?php

namespace z0s\Websocket\Api;

use Bloatless\WebSocket\Application\Application;
use Bloatless\WebSocket\Connection;

abstract class Websocket extends Application
{
    /**
     * Path that it listens to, no leading slash.
     *
     * @var string
     */
    protected string $hostPath = '';

    abstract public function onConnect(Connection $connection): void;

    abstract public function onDisconnect(Connection $connection): void;

    abstract public function onData(string $data, Connection $client): void;

    abstract public function onIPCData(array $data): void;

    public function getHost(): string
    {
        return $this->hostPath;
    }
}
